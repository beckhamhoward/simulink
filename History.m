%% Edit and changes file
% This file is to be used as a revision history for any changes or notes
% given to the control files.  All changes will be recoreded here and then
% uploaded to a backup.
%% January 5th 2016
% Made changes to the layout of the parameter file
% I need to find a copy of 2015b to use.
% 
% Later: I have purchased 2015b matlab and I can run the simulation.
%
%% January 6th 2016
% Today is the overview of the simulation project files.  I'll include any
% notes and change here.
% 
% I don't like the zone defense design for the robots. If they are about to
% score I want both robots playing defense.  Put the robot closest to the
% ball in front of the ball and put the other robot to the side of the
% closest opponent to the ball.
%
% For offense I want to push one robot forward to play a strong offense and
% put the other robot about 1/2 to 1/3 away from our goal in order to play
% defense but also be able to possibly run a surprise play on the goal.
%
% One strategy is to determine where to aim the ball at the goal depending
% on the position of the ball.
%
% PID is available on the hardware and we can implement this in a real
% world system.  Get the P controller working first then try to add other
% controlls to the system
%
% The robots when they collide it acts like a super spring.
%
% Changes:  I made some changes to the controller_away.m file.  I'll make
% the changes to this team in order to determined
% 
%