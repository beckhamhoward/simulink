# SIMULINK Robot Soccer #

These are the simulation files used for the 2016 Senior Robot Soccer Project

### Overview ###

* Included in these files are pre-built control files and animation for 4 robots and 1 ball playing on a set field.
* The simulation file will be used solely for design and implementation of the AI as all of the controls systems are in place.  A later simulation may be added to simulate the specific shape and behaviors of the BECKHMAN-HOWARD robots.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Use only MatLab 2015b due to conflicts with other versions of MatLab.
* After downloading the files open up the "param.m" file and run it to change the working directory.
* Open the file "robot_soccer_sim.slx" inside MatLab
* When the file loads click on the run button to run the simulation
* If there are no errors the simulation will run
* To make changes to the AI functions use the file "controller_away.m" and leave "controller_home.m" as a challenger to beat.

### Who do I talk to? ###

* If you have any questions please let Craig know.